**Projeto referente ao desafio WPensar para estágio desenvolvido por Samir Saadi**

O projeto consiste em um sistema web de mercados que cadastra produtos, compras, além de listá-las e calcular o preço médio de compra.

*Informações de tecnologias utilizadas e como rodar o projeto encontram-se abaixo.*

---

## Tecnologias principais utilizadas

1. Python 3.6.4+
2. Django 2.1.1
3. Bootstrap 4
4. Sqlite3
5. Html e CSS

---

## Instruções para rodar o projeto

Abaixo estão os passos a serem seguidos.

1. Clonar o repositório: git clone https://samirsaadi@bitbucket.org/samirsaadi/querotrabalharnawpensar-samirsaadi.git
2. Instalar o Python 3.6.4+ (https://www.python.org/downloads/).
3. Criar um ambiente virtual, conforme tutorial: https://tutorial.djangogirls.org/pt/django_installation/
4. Ativar o ambiente virtual.
5. Pelo terminal entrar na pasta do projeto clonado.
6. Utilizar o comando "pip install -r requirements.txt" para instalar todas as dependências do projeto.
7. Rodar o projeto através do comando "python manage.py runserver".

---

## Caso queiram acessar o admin do Django:

1. Entrar em localhost:8000/admin/
2. Logar com "admin".
3. Senha "adminwpensar".

---

## Criado por:

Samir Rabello Abcaran Saadi
samirsaadi@gmail.com